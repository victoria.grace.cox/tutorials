**Note, feel free to completely clear and restart this page in terms of content. But, DO NOT delete the file itself. As stated below, this is the homepage file (what's associated with the home button at the top of the static page directory). If you delete this file, that home page will not work!**

# Welcome to MkDocs

This is a test site hosted on [GitLab Pages](https://pages.gitlab.io). You can
[browse its source code](https://gitlab.com/pages/mkdocs), fork it and start
using it on your projects.

For full documentation visit [mkdocs.org](http://mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
